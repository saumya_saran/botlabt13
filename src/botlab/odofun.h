#ifndef __ODOFUN__
#define __ODOFUN__

#include <gsl/gsl_matrix.h>
#include <gsl/gsl_blas.h>

//reurns the xyt vector from the previous step
//pass theta a null if using 
void p_prime(double *xyt_step, double dr, double dl, double b, double theta, int gyro);

void variance(double *var, double alpha, double beta, double dl, double dr, long long int dt, int gyro);

gsl_matrix* calc_sigmaDelta(double *var, double b, int use_gyro);


#endif
