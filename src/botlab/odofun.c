#include "odofun.h"
#include <math.h>

void p_prime(double *xyt_step, double dr, double dl, double b, double theta, int gyro){

  //set values for using odometry, need to pass in theta as a null
  if (gyro == 0){
    xyt_step[0] = (dr+dl)/2;
    xyt_step[1] = 0;
    xyt_step[2] = (dr-dl)/b;
  }
  //set values for using the gyro
  else {
    xyt_step[0] = (dr+dl)/2;
    xyt_step[1] = 0;
    xyt_step[2] = theta;
  }

}

void variance(double *var, double alpha, double beta, double dl, double dr, long long int dt, int gyro){

  if (gyro == 0){
    var[0] = alpha * fabs(dl);
    var[1] = alpha * fabs(dr);
    var[2] = beta * fabs(dl-dr);
  }
  else {
    var[0] = alpha * fabs(dl);
    var[1] = alpha * fabs(dr);
    var[2] = beta * fabs(dl-dr);
    var[3] = dt * 0.1 / 1e6;
  }
}


gsl_matrix* calc_sigmaDelta(double *var, double b, int use_gyro){
  //for odometry calcs
  if (use_gyro == 0){
    //initilize necessary matrices
    gsl_matrix * sigma_Delta = gsl_matrix_calloc(3,3);
    gsl_matrix * A = gsl_matrix_calloc(3,3);
    gsl_matrix * B = gsl_matrix_calloc(3,3);
    gsl_matrix * var_M = gsl_matrix_calloc(3,3);


    //initialize values for the A matrix
    gsl_matrix_set( A, 0, 0, 0.5 );
    gsl_matrix_set( A, 0, 1, 0.5 );

    gsl_matrix_set( A, 1, 2, 1 );

    gsl_matrix_set( A, 2, 0, -1/b );
    gsl_matrix_set( A, 2, 1, 1/b );

    //initialize the variance matrix, it is a diaganol matrix where each element
    // is a variance of a parameter
    gsl_matrix_set( var_M, 0, 0, var[0] );
    gsl_matrix_set( var_M, 1, 1, var[1] );
    gsl_matrix_set( var_M, 2, 2, var[2] );


    // multiply A*var_M
    gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, A, var_M, 0.0, B);

    // multiple product of previous time A transpose
    gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, B, A, 0.0, sigma_Delta);
    return sigma_Delta;

  }

  else {  //run the gyro calculations
    //initilize necessary matrices
    gsl_matrix * sigma_Delta = gsl_matrix_calloc(3,3);
    gsl_matrix * A = gsl_matrix_calloc(3,4);
    gsl_matrix * B = gsl_matrix_calloc(3,4);
    gsl_matrix * var_M = gsl_matrix_calloc(4,4);

    //initialize values for the A matrix
    gsl_matrix_set( A, 0, 0, 0.5 );
    gsl_matrix_set( A, 0, 1, 0.5 );

    gsl_matrix_set( A, 1, 2, 1 );

    gsl_matrix_set( A, 2, 0, 1);

    //initialize the variance matrix, it is a diaganol matrix where each element
    // is a variance of a parameter
    gsl_matrix_set( var_M, 0, 0, var[0] );
    gsl_matrix_set( var_M, 1, 1, var[1] );
    gsl_matrix_set( var_M, 2, 2, var[2] );
    gsl_matrix_set( var_M, 3, 3, var[3] );


    // multiply A*var_M
    gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, A, var_M, 0.0, B);

    // multiple product of previous time A transpose
    gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, B, A, 0.0, sigma_Delta);
    return sigma_Delta;

  }
return NULL;
}

