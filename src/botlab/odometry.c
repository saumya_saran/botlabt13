#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <math.h>

#include "common/getopt.h"
#include "math/gsl_util_matrix.h"
#include "math/gsl_util_blas.h"
#include "math/math_util.h"

#include "lcmtypes/maebot_motor_feedback_t.h"
#include "lcmtypes/maebot_sensor_data_t.h"
#include "lcmtypes/pose_xyt_t.h"

#include "xyt.h"
#include "odofun.h"

#define ALPHA_STRING          "1.0"  // longitudinal covariance scaling factor
#define BETA_STRING           "1.0"  // lateral side-slip covariance scaling factor
#define GYRO_RMS_STRING       "1.0"    // [deg/s]

typedef struct state state_t;
struct state {
    getopt_t *gopt;

    lcm_t *lcm;
    const char *odometry_channel;
    const char *feedback_channel;
    const char *sensor_channel;

    // odometry params
    double meters_per_tick; // conversion factor that translates encoder pulses into linear wheel displacement
    double alpha;
    double beta;
    double gyro_rms;
    double b; //baseline

    bool use_gyro;
    int64_t dtheta_utime;
    double dtheta;
    double dtheta_sigma;

    double xyt[3]; // 3-dof pose
    double xyt_last[3]; // 3-dof pose
    double Sigma[3*3];
    double Sigma_last[3*3];

    double dl_tot;
    double dr_tot;


    //record hitorical encoder values
    double d_last[2];
    double enc_left;
    double enc_right;


    //gyro stuff
    long int gyro_count;
    long long int timeset[2];
    long long int valset[2];
    double slope;
    long long int g_last;
    long long int t_last;
    double theta;
    double d_theta;

    int start_flag;
    int start_flag2;
    int wait_flag;

    int left_init;
    int right_init;
    int encoder_right;
    int encoder_left;
};


static void
motor_feedback_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                        const maebot_motor_feedback_t *msg, void *user)
{
    state_t *state = user;

    if (state->wait_flag ==0){
      printf("odometry waiting for gyro\n");
      return;
    }

    state->enc_left = msg->encoder_left_ticks;
    state->enc_right = msg->encoder_right_ticks;

    if (state->start_flag==0){
      state->start_flag=1;
      state->left_init = msg->encoder_left_ticks;
      state->right_init = msg->encoder_right_ticks;
    }

    //needed so encoders get written to state so the gyro code can read it
    state->encoder_left = msg->encoder_left_ticks;
    state->encoder_right = msg->encoder_right_ticks;

    // entry of 0 should skip this part

    double dl, dr;
    double var[4];

    int i,j;
    state->b = 0.08;
    double xyt_step[3];

    // get the distance traveled by each wheel since the last step in meters
    dl = (double)(msg->encoder_left_ticks- state->left_init)/4613.0 - state->d_last[0]/4613.0 ;
    dr = (double)(msg->encoder_right_ticks- state->right_init)/4700.0 - state->d_last[1]/4700.0 ; 

    //find the total distance travelled by each wheel
    state->dl_tot = state->dl_tot + fabs(dl) ;
    state->dr_tot = state->dr_tot + fabs(dr) ;

    //calculate what the expected pose of the robot is
    p_prime(xyt_step, dr, dl, state->b, state->d_theta, state->use_gyro);

    //add displacement from the last step
    xyt_head2tail(state->xyt, NULL, state->xyt_last, xyt_step);

    //create the diaganol variance matrix
    variance(var, state->alpha, state->beta, dl, dr, (msg->utime_sama5 - state->timeset[1]),  state->use_gyro);

    printf("\nxyt = %lf %lf %lf \n", state->xyt[0], state->xyt[1], state->xyt[2]);

    //Calculate the sigma_delta matrix = A*var*A_transpose
    gsl_matrix * sigma_delta = gsl_matrix_calloc(3,3);
    sigma_delta = calc_sigmaDelta(var, state->b, state->use_gyro);


    //create the larger 6x6 matrix
    gsl_matrix * sigma6 = gsl_matrix_calloc(6,6);
    for (i=0; i<3; i++){
     for (j=0; j<3; j++){
        gsl_matrix_set( sigma6, i, j,   state->Sigma[i*3+j]    );
      }
    }
    for (i=3; i<6; i++){
      for (j=3; j<6; j++){
        gsl_matrix_set( sigma6, i, j,   gsl_matrix_get(sigma_delta,i-3,j-3)    );
      }
    }

    //calculate the jacobian and convert to a gsl
    double dummy[3];
    double jacobian_plus[3*6];

    xyt_head2tail(dummy, jacobian_plus, state->xyt_last, xyt_step);

    //fill the jacobian gsl
    gsl_matrix * jacobian_plus_gsl = gsl_matrix_calloc(3,6);
      for (i=0; i<6; i++){
        gsl_matrix_set( jacobian_plus_gsl, 0, i, jacobian_plus[i]  );
      }
      for (i=6; i<12; i++){
        gsl_matrix_set( jacobian_plus_gsl, 1, i-6, jacobian_plus[i]  );
      }
      for (i=12; i<18; i++){
        gsl_matrix_set( jacobian_plus_gsl, 2, i-12, jacobian_plus[i]  );
      }

    //do the final calculation
    gsl_matrix * sigma_p_prime = gsl_matrix_calloc(3,3);
    gsl_matrix * middle = gsl_matrix_calloc(3,6);
    gsl_blas_dgemm (CblasNoTrans, CblasNoTrans, 1.0, jacobian_plus_gsl, sigma6, 0.0, middle );

    gsl_blas_dgemm (CblasNoTrans, CblasTrans, 1.0, middle, jacobian_plus_gsl, 0.0, sigma_p_prime);

   //convert back to an array form
      for (i=0; i<3; i++){
       for (j=0; j<3; j++){
        state->Sigma[i*3+j] = gsl_matrix_get(sigma_p_prime,i,j);
        printf(" %lf ",state->Sigma[i*3+j]);
       }
      }


    //write the current variable into the last variable in state
    state->d_last[0] = msg->encoder_left_ticks-state->left_init;
    state->d_last[1] = msg->encoder_right_ticks-state->right_init;


    //write to all of the "last" values in state
    for (i=0;i<9;i++){
       state->Sigma_last[i] = state->Sigma[i];
    }

    state->xyt_last[0] = state->xyt[0];
    state->xyt_last[1] = state->xyt[1];
    state->xyt_last[2] = state->xyt[2];

    //clean up vectors
    gsl_matrix_free(sigma6);
    gsl_matrix_free(sigma_p_prime);
    gsl_matrix_free(middle);
    gsl_matrix_free(sigma_delta);

    // publish
    pose_xyt_t odo = { .utime = msg->utime };
    memcpy (odo.xyt, state->xyt, sizeof state->xyt);
    memcpy (odo.Sigma, state->Sigma, sizeof state->Sigma);
    pose_xyt_t_publish (state->lcm, state->odometry_channel, &odo);
}


                                                       
                                                       



static void
sensor_data_handler (const lcm_recv_buf_t *rbuf, const char *channel,
                     const maebot_sensor_data_t *msg, void *user)
{
    state_t *state = user;

    if (state->use_gyro==0)
        return;

// Log gyro data routine if needed
//    FILE *f;
//    char filename[] = "../logs/newfile.txt";
//    f = fopen(filename, "a");


    if (state->start_flag2==0){
      state->gyro_count = 0;
      state->start_flag2 = 1;
      state->timeset[0] = msg->utime_sama5;
      state->valset[0] = msg->gyro_int[2];
//      fprintf(f, "1time, 2gyro_int[0], 3gyro_int[1], 4gyro_int[2], 5gyro[1], 6gyro[2], 7gyro[3], 9theta , 10slope, 11bias, \n"); 

    }
/*
    if (f == NULL) {
        printf("nulled");
        return; 
    }

    fprintf(f, "%lld, %lld, %lld, %lld, ", msg->utime_sama5, msg->gyro_int[0], msg->gyro_int[1], msg->gyro_int[2]);
    fprintf(f, "%d, %d, %d, ", msg->gyro[0], msg->gyro[1], msg->gyro[2]);
*/


    //read in ~ a hunder gyro_int points to calc the bias from
    //double if statement ensures that (1) the integer count does not reset itself to 0
    // and (2) at around a hundred we get our second point to do the bias calc from.
    if (state->gyro_count < 100){
      state->theta = 0;
      state->gyro_count++;
      printf("gyro count: %ld\n", state->gyro_count);
      if (state->gyro_count == 99){
        state->timeset[1] = msg->utime_sama5;
        state->valset[1] = msg->gyro_int[2];

        state->slope = (double)(state->valset[1] - state -> valset[0])  / 
                       (double)(state->timeset[1] - state->timeset[0]);
        state->theta = 0;
        state->wait_flag = 1;
//      state->xyt[2] = 0;
//      printf("calculate slope");

      }
    }


    //printf("time interval: %lld   %lld\n", state->timeset[0], state->timeset[1]);
    //printf("vlaue interval: %lld   %lld\n", state->valset[0], state->valset[1]);
    //printf("slope: %lf\n", state->slope);

    long long int delta_t;
    //long long int d_theta;
    long long int bias;


    delta_t = msg->utime_sama5 - state->t_last;
//printf("delta_t = %lld  ", delta_t);

    bias = state->slope * (double)delta_t;
//printf(" bias = %lld  ", bias);

//printf("\ncurrent reading = %lld    last reading = %lld  \n", msg->gyro_int[2], state->g_last);
    state->d_theta = ((double)msg->gyro_int[2] - (double)state->g_last - bias)*(1e-6 / 131) * 0.0174532  ;
//printf("delta_theta = %lld  ", delta_theta);


    state->theta = state->theta + state->d_theta; 

    printf("state->theta = %lf  \n\n", state->theta);

//    fprintf(f, "%lf, %lf, %lld, ", state->theta, state->slope, bias);
//    fprintf(f,"\n");


    state->g_last = msg->gyro_int[2];
    state->t_last = msg->utime_sama5;


//    fclose(f);
}


int
main (int argc, char *argv[])
{
    // so that redirected stdout won't be insanely buffered.
    setvbuf (stdout, (char *) NULL, _IONBF, 0);

    state_t *state = calloc (1, sizeof *state);

    state->meters_per_tick = 1.0; // IMPLEMENT ME

    state->gopt = getopt_create ();
    getopt_add_bool   (state->gopt, 'h', "help", 0, "Show help");
    getopt_add_bool   (state->gopt, 'g', "use-gyro", 0, "Use gyro for heading instead of wheel encoders");
    getopt_add_string (state->gopt, '\0', "odometry-channel", "BOTLAB_ODOMETRY", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "feedback-channel", "MAEBOT_MOTOR_FEEDBACK", "LCM channel name");
    getopt_add_string (state->gopt, '\0', "sensor-channel", "MAEBOT_SENSOR_DATA", "LCM channel name");
    getopt_add_double (state->gopt, '\0', "alpha", ALPHA_STRING, "Longitudinal covariance scaling factor");
    getopt_add_double (state->gopt, '\0', "beta", BETA_STRING, "Lateral side-slip covariance scaling factor");
    getopt_add_double (state->gopt, '\0', "gyro-rms", GYRO_RMS_STRING, "Gyro RMS deg/s");

    if (!getopt_parse (state->gopt, argc, argv, 1) || getopt_get_bool (state->gopt, "help")) {
        printf ("Usage: %s [--url=CAMERAURL] [other options]\n\n", argv[0]);
        getopt_do_usage (state->gopt);
        exit (EXIT_FAILURE);
    }

    state->use_gyro = getopt_get_bool (state->gopt, "use-gyro");
    state->odometry_channel = getopt_get_string (state->gopt, "odometry-channel");
    state->feedback_channel = getopt_get_string (state->gopt, "feedback-channel");
    state->sensor_channel = getopt_get_string (state->gopt, "sensor-channel");
    state->alpha = getopt_get_double (state->gopt, "alpha");
    state->beta = getopt_get_double (state->gopt, "beta");
    state->gyro_rms = getopt_get_double (state->gopt, "gyro-rms") * DTOR;
    state->start_flag = 0;
    state->start_flag2 = 0;
    state->alpha = 0.00368;
    state->beta = 0.00675;
    state->use_gyro = 0; //1 triggers the gyro routine, 0 triggers odometry
    if (state->use_gyro == 0){
      state->wait_flag = 1;
    }
    else{
      state->wait_flag = 0;
    }

    // initialize LCM
    state->lcm = lcm_create (NULL);
    maebot_motor_feedback_t_subscribe (state->lcm, state->feedback_channel,
                                       motor_feedback_handler, state);
    maebot_sensor_data_t_subscribe (state->lcm, state->sensor_channel,
                                    sensor_data_handler, state);

    printf ("ticks per meter: %f\n", 1.0/state->meters_per_tick);

    while (1)
        lcm_handle (state->lcm);
}
